import { mockRanges } from './mocks';
import { FastPrioritySet } from './priority.set.fast';
import { NaivePrioritySet } from './priority.set.naive';
import jsonexport from 'jsonexport';
import { writeFileSync } from 'fs';
import { benchmark } from './bench';
benchmark("/Users/matanshaked/development/rust/hello_world/intervals.json")

// benchmarkDifferentAmounts();

function benchmarkDifferentAmounts() {
    const REPETITIONS = 6;
    const amounts = Array.from({ length: 300 }).map((_, idx) => idx * 100);
    const uniteTwoRangesWithAmount = (amount: number): { amount: number; durationNaive: number; durationFast: number; } => {
        const ranges1 = mockRanges(amount);
        const ranges2 = mockRanges(amount);
        const start = new Date().getTime();
        const set1 = new FastPrioritySet(ranges1.map(x => ({ data: {}, priority: 0, start: x.start, end: x.end })));
        const set2 = new FastPrioritySet(ranges2.map(x => ({ data: {}, priority: 1, start: x.start, end: x.end })));
        const union = set1.unite(set2);
        const end = new Date().getTime();
        const startNaive = new Date().getTime();
        const set1Naive = new NaivePrioritySet(ranges1.map(x => ({ data: {}, priority: 0, start: x.start, end: x.end })));
        const set2Naive = new NaivePrioritySet(ranges2.map(x => ({ data: {}, priority: 1, start: x.start, end: x.end })));
        const unionNaive = set1Naive.unite(set2Naive);
        const endNaive = new Date().getTime();

        return {
            amount,
            durationNaive: endNaive - startNaive,
            durationFast: end - start
            // unionNaive,
            // unionFast: union
        };
    };
    const results = amounts.map(amount => {
        console.log(`processing amount ${amount}`);

        const repetitionsResults = Array.from({ length: REPETITIONS }).map(() => uniteTwoRangesWithAmount(amount));
        const totalRepetitionsResults = repetitionsResults.reduce((acc, curr) => ({
            amount: curr.amount,
            durationFast: acc.durationFast + curr.durationFast,
            durationNaive: acc.durationNaive + curr.durationNaive
        }));
        return {
            amount,
            durationFast: totalRepetitionsResults.durationFast / REPETITIONS,
            durationNaive: totalRepetitionsResults.durationNaive / REPETITIONS
        };
    });

    jsonexport(results, (err, csv) => {
        writeFileSync("./results.csv", csv);
    });

    results.map(result => {
        console.log(`amount=${result.amount}
    fast took ${result.durationFast} ms
    naive took ${result.durationNaive} ms
    `);
    });

    const totalDurations = results.reduce((acc, curr) => ({
        totalDurationFast: acc.totalDurationFast + curr.durationFast,
        totalDurationNaive: acc.totalDurationNaive + curr.durationNaive
    }), { totalDurationNaive: 0, totalDurationFast: 0 });

    console.log(`avg fast result is ${totalDurations.totalDurationFast / results.length}`);
    console.log(`avg naive result is ${totalDurations.totalDurationNaive / results.length}`);
}
