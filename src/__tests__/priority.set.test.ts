// import { FastPrioritySet } from "../priority.set.fast";
import { NaivePrioritySet } from "../priority.set.naive";
import { HasData, HasPriority, Range } from "../range";

describe("Fast Priority Set", () => {
    describe("unite", () => {
        describe("one range in each set", () => {
            test("disjoint ranges => returns two ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 20, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 40, end: 50, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(2);
                expect(union[0]).toEqual(ranges1[0]);
                expect(union[1]).toEqual(ranges2[0]);
            });
            test("intersecting ranges, higher priority first => returns two ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 20, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 15, end: 30, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(2);
                expect(union[0].start).toEqual(ranges1[0].start);
                expect(union[0].end).toEqual(ranges1[0].end);
                expect(union[1].start).toEqual(ranges1[0].end);
                expect(union[1].end).toEqual(ranges2[0].end);
            });
            test("intersecting ranges, lower priority first => returns two ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 15, end: 30, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 20, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(2);
                expect(union[0].start).toEqual(ranges2[0].start);
                expect(union[0].end).toEqual(ranges1[0].start);
                expect(union[1].start).toEqual(ranges1[0].start);
                expect(union[1].end).toEqual(ranges1[0].end);
            });

            test("higher priority range contained in lower priority range => returns three ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 15, end: 30, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(3);
                expect(union[0].start).toEqual(ranges2[0].start);
                expect(union[0].end).toEqual(ranges1[0].start);
                expect(union[1].start).toEqual(ranges1[0].start);
                expect(union[1].end).toEqual(ranges1[0].end);
                expect(union[2].start).toEqual(ranges1[0].end);
                expect(union[2].end).toEqual(ranges2[0].end);
            });

            test("higher priority range contains lower priority range => returns one range", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 20, end: 30, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(1);
                expect(union[0].start).toEqual(ranges1[0].start);
                expect(union[0].end).toEqual(ranges1[0].end);
            });

            test("higher priority range contained in and starts with lower priority range => returns two ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 30, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(2);
                expect(union[0].start).toEqual(ranges1[0].start);
                expect(union[0].end).toEqual(ranges1[0].end);
                expect(union[1].start).toEqual(ranges1[0].end);
                expect(union[1].end).toEqual(ranges2[0].end);
            });

            test("higher priority range contains and starts with lower priority range => returns one range", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 20, end: 30, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(1);
                expect(union[0].start).toEqual(ranges1[0].start);
                expect(union[0].end).toEqual(ranges1[0].end);
            });

            test("higher priority range contained in and ends with lower priority range => returns two ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 30, end: 50, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(2);
                expect(union[0].start).toEqual(ranges2[0].start);
                expect(union[0].end).toEqual(ranges1[0].start);
                expect(union[1].start).toEqual(ranges1[0].start);
                expect(union[1].end).toEqual(ranges1[0].end);
            });

            test("higher priority range contains and ends with lower priority range => returns one range", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 30, end: 30, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(1);
                expect(union[0].start).toEqual(ranges1[0].start);
                expect(union[0].end).toEqual(ranges1[0].end);
            });
        });
        describe("two ranges in each set", () => {
            test("two ranges intersect and two disjoint => returns 4 ranges", () => {
                const ranges1: (Range & HasData<{}> & HasPriority)[] = [{ start: 10, end: 50, data: {}, priority: 0 }, { start: 100, end: 120, data: {}, priority: 0 }];
                const ranges2: (Range & HasData<{}> & HasPriority)[] = [{ start: 20, end: 70, data: {}, priority: 1 }, { start: 80, end: 90, data: {}, priority: 1 }];
                const set1 = new NaivePrioritySet(ranges1);
                const set2 = new NaivePrioritySet(ranges2);

                const union = set1.unite(set2).toArray();

                expect(union).toHaveLength(4);
                expect(union[0].start).toEqual(ranges1[0].start);
                expect(union[0].end).toEqual(ranges1[0].end);
                expect(union[1].start).toEqual(ranges1[0].end);
                expect(union[1].end).toEqual(ranges2[0].end);
                expect(union[2].start).toEqual(ranges2[1].start);
                expect(union[2].end).toEqual(ranges2[1].end);
                expect(union[3].start).toEqual(ranges1[1].start);
                expect(union[3].end).toEqual(ranges1[1].end);
            });
        });
    });
    describe("counter examples", () => {
        test("three ranges in each set", () => {
            const ranges1: (Range & HasData<{}> & HasPriority)[] = [{"start":3,"end":7,data:{},priority:0},{"start":5,"end":8,data:{},priority:0},{"start":3,"end":4,data:{},priority:0}];
            const ranges2: (Range & HasData<{}> & HasPriority)[] = [{"start":6,"end":6,data:{},priority:1},{"start":3,"end":7,data:{},priority:1},{"start":9,"end":14,data:{},priority:1}];
            const set1 = new NaivePrioritySet(ranges1);
            const set2 = new NaivePrioritySet(ranges2);

            const union = set1.unite(set2).toArray();
            expect(union).toEqual([
                expect.objectContaining({start:3,end:7,priority:0}),
                expect.objectContaining({start:7,end:8,priority:0}),
                expect.objectContaining({start:9,end:14,priority:1}),
            ])
        });
    });
});