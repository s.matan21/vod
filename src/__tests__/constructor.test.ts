import { FastPrioritySet } from '../priority.set.fast';

test("1", () => {
    const ranges = [{ "start": 255402.8509635824, "end": 519178.94507809146 }, { "start": 829196.1694970038, "end": 957200.1120171675 }, { "start": 759845.7792525028, "end": 1261731.7333864905 }];
    const set = new FastPrioritySet(ranges.map(x => ({ data: {}, priority: 0, start: x.start, end: x.end })));
    expect(set.toArray()).toEqual([
        expect.objectContaining({ "start": 255402.8509635824, "end": 519178.94507809146 }),
        expect.objectContaining({ "start": 759845.7792525028, "end": 1261731.7333864905 }),
    ]);
});
test("1", () => {
    const ranges = [{ "start": 66461.23041536534, "end": 410817.3837238733 }, { "start": 204363.51983733237, "end": 753723.9363225673 }, { "start": 215828.11823528324, "end": 581783.6501992021 }];
    const set = new FastPrioritySet(ranges.map(x => ({ data: {}, priority: 0, start: x.start, end: x.end })));
    expect(set.toArray()).toEqual([
        expect.objectContaining({ "start": 66461.23041536534, "end": 410817.3837238733 }),
        expect.objectContaining({ "start":  410817.3837238733, "end":  753723.9363225673 }),
    ]);
});