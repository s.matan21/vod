export interface Range {
    start: number;
    end: number;
}
export interface HasData<T> {
    data: T;
}
export interface HasPriority {
    priority: number;
}


export const isIntersecting = (range1: Range, range2: Range): boolean =>
    Math.max(range1.start, range2.start) <= Math.min(range1.end, range2.end);
export const isStrictlyContainedIn = (range1: Range, range2: Range): boolean =>
    range1.start > range2.start && range1.end < range2.end;
export const isLooslyContainedIn = (range1: Range, range2: Range): boolean =>
    range1.start >= range2.start && range1.end <= range2.end;

export const endsBefore = (range1: Range, range2: Range): boolean =>
    range1.end <= range2.end;

export const subtract = <T>(range1: Range & HasData<T> & HasPriority, range2: Range): (Range & HasData<T> & HasPriority)[] =>
    isLooslyContainedIn(range1, range2) ?
        [] :
        isStrictlyContainedIn(range2, range1) ?
            [
                { ...range1, start: range1.start, end: range2.start },
                { ...range1, start: range2.end, end: range1.end }
            ] :
            isIntersecting(range1, range2) ? (
                range1.start < range2.start ?
                    [{ ...range1, end: range2.start }] :
                    [{ ...range1, start: range2.end }]
            ) :
                [range1];

export const isStartBeforeEnd = (range: Range): boolean =>
    range.start < range.end;