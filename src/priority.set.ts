import { HasData, HasPriority, Range } from "./range";

export interface PrioritySet<T> {
    unite(otherSet: PrioritySet<T>): PrioritySet<T>;
    toArray(): (Range & HasData<T> & HasPriority)[];
}