import { HasData, HasPriority, Range } from "./range";
import { FastPrioritySet } from './priority.set.fast';

interface IntervalsInput {
    first_set: Interval[];
    second_set: Interval[];
}
interface Interval extends Range { }

export const benchmark = (inputpath: string) => {
    const input = require(inputpath) as IntervalsInput;
    const firstSetRanges = input.first_set.map(intervalToRangeWithPriority(0));
    const secondSetRanges = input.first_set.map(intervalToRangeWithPriority(1));
    const start = new Date().getTime();
    const set1 = new FastPrioritySet(firstSetRanges);
    const set2 = new FastPrioritySet(secondSetRanges);
    const union = set1.unite(set2);
    const end = new Date().getTime();
    console.log(`length: ${union.toArray().length}`);
    console.log(`duration: ${end-start}ms`);
    
};

const intervalToRangeWithPriority = (priority: number) => (interval: Interval): (Range & HasData<{}> & HasPriority) => ({
    start: interval.start,
    end: interval.end,
    data: {},
    priority
});