import { Range } from "./range";

const MIN = 0;
const MAX = 100000;
const MAX_DURATION = 6000;

export const mockRanges = (amount: number): Range[] =>
    Array.from({ length: amount }).map(randomRange);

const randomRange = (): Range => {
    const start = Math.floor((Math.random() * (MAX - MIN)) + MIN);
    const length = Math.floor(Math.random() * MAX_DURATION);
    return {
        start,
        end: start + length
    };
};