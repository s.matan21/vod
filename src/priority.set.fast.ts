import { ascend, drop, dropLast, isNil, last, prop, sortBy, splitAt, } from "ramda";
import { PrioritySet } from "./priority.set";
import { endsBefore, HasData, HasPriority, isIntersecting, isStartBeforeEnd, isStrictlyContainedIn, Range, subtract } from "./range";

export class FastPrioritySet<T> implements PrioritySet<T> {
    private union: Range[];
    private ranges: (Range & HasData<T> & HasPriority)[];
    constructor(unsortedPossiblyIntersectingRanges: (Range & HasData<T> & HasPriority)[]) {
        const sorted = unsortedPossiblyIntersectingRanges.sort(ascend(range => range.start));
        this.union = this.uniteIntersectingRanges(sorted);
        this.ranges = this.unIntersectRanges(sorted);
    }
    toArray(): (Range & HasData<T> & HasPriority)[] {
        return this.ranges;
    }

    unite(otherSet: PrioritySet<T>): PrioritySet<T> {
        const rangesInOtherSetAndNotInCurrentSet = this.subtractUnionFrom(otherSet);
        return new FastPrioritySet(this.ranges.concat(rangesInOtherSetAndNotInCurrentSet));
    }

    private subtractUnionFrom(subtractee: PrioritySet<T>): (Range & HasData<T> & HasPriority)[] {
        const subtraction: (Range & HasData<T> & HasPriority)[] = [];
        let currIdxInUnion = 0;
        for (let currIdxInSubtractee = 0; currIdxInSubtractee < subtractee.toArray().length; currIdxInSubtractee++) {
            let currRangeToBeSubtractedFrom = subtractee.toArray()[currIdxInSubtractee];
            currIdxInUnion = this.findNextIntersectingRangeInUnion(currIdxInUnion, currRangeToBeSubtractedFrom);
            while (!isNil(currRangeToBeSubtractedFrom) && currIdxInUnion < this.union.length && isIntersecting(this.union[currIdxInUnion], currRangeToBeSubtractedFrom)) {
                const [s1, s2] = subtract(currRangeToBeSubtractedFrom, this.union[currIdxInUnion]);
                if (s2) {
                    subtraction.push(s1);
                    currRangeToBeSubtractedFrom = s2;
                }
                else {
                    currRangeToBeSubtractedFrom = s1;
                }
                currIdxInUnion++;
            }
            subtraction.push(currRangeToBeSubtractedFrom);
        }
        return subtraction.filter(s => !isNil(s));
    }

    private findNextIntersectingRangeInUnion(idx: number, range: Range): number {
        let nextIntersecting = idx;
        while (nextIntersecting < this.union.length && !isIntersecting(range, this.union[nextIntersecting])) {
            nextIntersecting++;
        }
        return nextIntersecting;
    }


    private uniteIntersectingRanges(sorted: (Range & HasData<T> & HasPriority)[]): Range[] {
        return sorted.reduce<Range[]>((union, currRange) => {
            const lastInUnion = last(union);
            if (lastInUnion) {
                if (isIntersecting(lastInUnion, currRange)) {
                    const unionWithoutLast = dropLast(1, union);
                    const newLast: Range = {
                        start: lastInUnion.start,
                        end: currRange.end
                    };
                    return unionWithoutLast.concat([newLast]);
                }
                return union.concat(currRange);
            }
            return [currRange];
        }, []);
    }
    private unIntersectRanges(sorted: (Range & HasData<T> & HasPriority)[]): (Range & HasData<T> & HasPriority)[] {
        let previousRange: (Range & HasData<T> & HasPriority);
        return sorted.reduce<(Range & HasData<T> & HasPriority)[]>((union, currRange) => {
            const lastInUnion = last(union);
            if (lastInUnion) {
                const currRangeWithoutPreviousRange = subtract(currRange, previousRange);
                previousRange = currRange.end < previousRange.end ?
                    previousRange :
                    currRange;
                if (currRangeWithoutPreviousRange.length == 0) {
                    return union;
                }
                if (currRangeWithoutPreviousRange.length == 1) {
                    return union.concat([currRangeWithoutPreviousRange[0]]);
                }
            }
            previousRange = currRange;
            return [currRange];
        }, []).filter(isStartBeforeEnd);
    }


};