import { ascend, concat, drop, dropLast, isEmpty, isNil, last, prop, splitAt, } from "ramda";
import { PrioritySet } from "./priority.set";
import { endsBefore, HasData, HasPriority, isIntersecting, isStrictlyContainedIn, Range, subtract } from "./range";

export class NaivePrioritySet<T> implements PrioritySet<T> {
    private union: Range[];
    private ranges: (Range & HasData<T> & HasPriority)[];
    constructor(unsortedPossiblyIntersectingRanges: (Range & HasData<T> & HasPriority)[]) {
        const sorted = unsortedPossiblyIntersectingRanges.sort(ascend(range => range.start));
        this.union = this.uniteIntersectingRanges(sorted);
        this.ranges = this.unIntersectRanges(sorted);
    }
    toArray(): (Range & HasData<T> & HasPriority)[] {
        return this.ranges;
    }

    unite(otherSet: PrioritySet<T>): PrioritySet<T> {
        const rangesInOtherSetAndNotInCurrentSet = this.subtractUnionFrom(otherSet);
        return new NaivePrioritySet(this.ranges.concat(rangesInOtherSetAndNotInCurrentSet));
    }

    private subtractUnionFrom(subtractee: PrioritySet<T>): (Range & HasData<T> & HasPriority)[] {
        return subtractee.toArray().map(range => {
            const intersecting = this.union.filter(r => isIntersecting(r, range));
            if(isEmpty(intersecting)){
                return [range]
            }
            return intersecting.map(r => subtract(range, r)).reduce(concat, []);
        }).reduce(concat,[]);
    }


    private uniteIntersectingRanges(sorted: (Range & HasData<T> & HasPriority)[]): Range[] {
        return sorted.reduce<Range[]>((union, currRange) => {
            const lastInUnion = last(union);
            if (lastInUnion) {
                if (isIntersecting(lastInUnion, currRange)) {
                    const unionWithoutLast = dropLast(1, union);
                    const newLast: Range = {
                        start: lastInUnion.start,
                        end: currRange.end
                    };
                    return unionWithoutLast.concat([newLast]);
                }
                return union.concat(currRange);
            }
            return [currRange];
        }, []);
    }
    private unIntersectRanges(sorted: (Range & HasData<T> & HasPriority)[]): (Range & HasData<T> & HasPriority)[] {
        let previousRange: (Range & HasData<T> & HasPriority);
        return sorted.reduce<(Range & HasData<T> & HasPriority)[]>((union, currRange) => {
            const lastInUnion = last(union);
            if (lastInUnion) {
                const currRangeWithoutPreviousRange = subtract(currRange, previousRange);
                previousRange = currRange;
                if (currRangeWithoutPreviousRange.length == 0) {
                    return union;
                }
                if (currRangeWithoutPreviousRange.length == 1) {
                    return union.concat([currRangeWithoutPreviousRange[0]]);
                }
            }
            previousRange = currRange;
            return [currRange];
        }, []);
    }


};