"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.benchmark = void 0;
const priority_set_fast_1 = require("./priority.set.fast");
const benchmark = (inputpath) => {
    const input = require(inputpath);
    const firstSetRanges = input.first_set.map(intervalToRangeWithPriority(0));
    const secondSetRanges = input.first_set.map(intervalToRangeWithPriority(1));
    const start = new Date().getTime();
    const set1 = new priority_set_fast_1.FastPrioritySet(firstSetRanges);
    const set2 = new priority_set_fast_1.FastPrioritySet(secondSetRanges);
    const union = set1.unite(set2);
    const end = new Date().getTime();
    console.log(`length: ${union.toArray().length}`);
    console.log(`duration: ${end - start}ms`);
};
exports.benchmark = benchmark;
const intervalToRangeWithPriority = (priority) => (interval) => ({
    start: interval.start,
    end: interval.end,
    data: {},
    priority
});
//# sourceMappingURL=bench.js.map