"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isStartBeforeEnd = exports.subtract = exports.endsBefore = exports.isLooslyContainedIn = exports.isStrictlyContainedIn = exports.isIntersecting = void 0;
const isIntersecting = (range1, range2) => Math.max(range1.start, range2.start) <= Math.min(range1.end, range2.end);
exports.isIntersecting = isIntersecting;
const isStrictlyContainedIn = (range1, range2) => range1.start > range2.start && range1.end < range2.end;
exports.isStrictlyContainedIn = isStrictlyContainedIn;
const isLooslyContainedIn = (range1, range2) => range1.start >= range2.start && range1.end <= range2.end;
exports.isLooslyContainedIn = isLooslyContainedIn;
const endsBefore = (range1, range2) => range1.end <= range2.end;
exports.endsBefore = endsBefore;
const subtract = (range1, range2) => exports.isLooslyContainedIn(range1, range2) ?
    [] :
    exports.isStrictlyContainedIn(range2, range1) ?
        [
            Object.assign(Object.assign({}, range1), { start: range1.start, end: range2.start }),
            Object.assign(Object.assign({}, range1), { start: range2.end, end: range1.end })
        ] :
        exports.isIntersecting(range1, range2) ? (range1.start < range2.start ?
            [Object.assign(Object.assign({}, range1), { end: range2.start })] :
            [Object.assign(Object.assign({}, range1), { start: range2.end })]) :
            [range1];
exports.subtract = subtract;
const isStartBeforeEnd = (range) => range.start < range.end;
exports.isStartBeforeEnd = isStartBeforeEnd;
//# sourceMappingURL=range.js.map