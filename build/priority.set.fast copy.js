"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FastPrioritySet = void 0;
const ramda_1 = require("ramda");
const range_1 = require("./range");
class FastPrioritySet {
    constructor(unsortedPossiblyIntersectingRanges) {
        const sorted = unsortedPossiblyIntersectingRanges.sort(ramda_1.ascend(range => range.start));
        this.union = this.uniteIntersectingRanges(sorted);
        this.ranges = this.unIntersectRanges(sorted);
    }
    toArray() {
        return this.ranges;
    }
    unite(otherSet) {
        const rangesInOtherSetAndNotInCurrentSet = this.subtractUnionFrom(otherSet);
        return new FastPrioritySet(this.ranges.concat(rangesInOtherSetAndNotInCurrentSet));
    }
    subtractUnionFrom(subtractee) {
        const subtraction = [];
        let currIdxInUnion = 0;
        for (let currIdxInSubtractee = 0; currIdxInSubtractee < subtractee.toArray().length; currIdxInSubtractee++) {
            let currRangeToBeSubtractedFrom = subtractee.toArray()[currIdxInSubtractee];
            currIdxInUnion = this.findNextIntersectingRangeInUnion(currIdxInUnion, currRangeToBeSubtractedFrom);
            while (!ramda_1.isNil(currRangeToBeSubtractedFrom) && currIdxInUnion < this.union.length && range_1.isIntersecting(this.union[currIdxInUnion], currRangeToBeSubtractedFrom)) {
                const [s1, s2] = range_1.subtract(currRangeToBeSubtractedFrom, this.union[currIdxInUnion]);
                if (s2) {
                    subtraction.push(s1);
                    currRangeToBeSubtractedFrom = s2;
                }
                else {
                    currRangeToBeSubtractedFrom = s1;
                }
                currIdxInUnion++;
            }
            subtraction.push(currRangeToBeSubtractedFrom);
        }
        return subtraction.filter(s => !ramda_1.isNil(s));
    }
    findNextIntersectingRangeInUnion(idx, range) {
        let nextIntersecting = idx;
        while (nextIntersecting < this.union.length && !range_1.isIntersecting(range, this.union[nextIntersecting])) {
            nextIntersecting++;
        }
        return nextIntersecting;
    }
    // private subtractUnionFrom(subtractee: PrioritySet<T>): (Range & HasData<T> & HasPriority)[] {
    //     const subtracteeRanges = [...subtractee.toArray()];
    //     const unionRanges = [...this.union];
    //     let subtraction: (Range & HasData<T> & HasPriority)[] = [];
    //     let idxInSubtractee = 0;
    //     let idxInUnion = 0;
    //     while (idxInSubtractee < subtracteeRanges.length && idxInUnion < unionRanges.length) {
    //         const currInSubtractee: (Range & HasData<T> & HasPriority) = subtracteeRanges[idxInSubtractee];
    //         const currInUnion: Range = unionRanges[idxInUnion];
    //         if (isIntersecting(currInSubtractee, currInUnion)) {
    //             const currSubtraction = subtract(currInSubtractee, currInUnion);
    //             if (endsBefore(currInSubtractee, currInUnion)) {
    //                 subtraction = subtraction.concat(currSubtraction);
    //             }
    //             if (endsBefore(currInUnion, currInSubtractee)) {
    //                 const [first, second] = currSubtraction;
    //                 subtracteeRanges[idxInSubtractee] = second ?? first;
    //                 if (second) {
    //                     subtraction.push(first);
    //                 }
    //             }
    //         }
    //         if (endsBefore(currInSubtractee, currInUnion)) {
    //             idxInSubtractee++;
    //         }
    //         if (endsBefore(currInUnion, currInSubtractee)) {
    //             idxInUnion++;
    //         }
    //     }
    //     const remainingInSubtractee = drop(idxInSubtractee, subtracteeRanges);
    //     return subtraction.concat(remainingInSubtractee);
    // }
    uniteIntersectingRanges(sorted) {
        return sorted.reduce((union, currRange) => {
            const lastInUnion = ramda_1.last(union);
            if (lastInUnion) {
                if (range_1.isIntersecting(lastInUnion, currRange)) {
                    const unionWithoutLast = ramda_1.dropLast(1, union);
                    const newLast = {
                        start: lastInUnion.start,
                        end: currRange.end
                    };
                    return unionWithoutLast.concat([newLast]);
                }
                return union.concat(currRange);
            }
            return [currRange];
        }, []);
    }
    unIntersectRanges(sorted) {
        let previousRange;
        return sorted.reduce((union, currRange) => {
            const lastInUnion = ramda_1.last(union);
            if (lastInUnion) {
                const currRangeWithoutPreviousRange = range_1.subtract(currRange, previousRange);
                previousRange = currRange;
                if (currRangeWithoutPreviousRange.length == 0) {
                    return union;
                }
                if (currRangeWithoutPreviousRange.length == 1) {
                    return union.concat([currRangeWithoutPreviousRange[0]]);
                }
            }
            previousRange = currRange;
            return [currRange];
        }, []);
    }
}
exports.FastPrioritySet = FastPrioritySet;
;
//# sourceMappingURL=priority.set.fast%20copy.js.map