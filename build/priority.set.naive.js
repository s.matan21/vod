"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.NaivePrioritySet = void 0;
const ramda_1 = require("ramda");
const range_1 = require("./range");
class NaivePrioritySet {
    constructor(unsortedPossiblyIntersectingRanges) {
        const sorted = unsortedPossiblyIntersectingRanges.sort(ramda_1.ascend(range => range.start));
        this.union = this.uniteIntersectingRanges(sorted);
        this.ranges = this.unIntersectRanges(sorted);
    }
    toArray() {
        return this.ranges;
    }
    unite(otherSet) {
        const rangesInOtherSetAndNotInCurrentSet = this.subtractUnionFrom(otherSet);
        return new NaivePrioritySet(this.ranges.concat(rangesInOtherSetAndNotInCurrentSet));
    }
    subtractUnionFrom(subtractee) {
        return subtractee.toArray().map(range => {
            const intersecting = this.union.filter(r => range_1.isIntersecting(r, range));
            if (ramda_1.isEmpty(intersecting)) {
                return [range];
            }
            return intersecting.map(r => range_1.subtract(range, r)).reduce(ramda_1.concat, []);
        }).reduce(ramda_1.concat, []);
    }
    uniteIntersectingRanges(sorted) {
        return sorted.reduce((union, currRange) => {
            const lastInUnion = ramda_1.last(union);
            if (lastInUnion) {
                if (range_1.isIntersecting(lastInUnion, currRange)) {
                    const unionWithoutLast = ramda_1.dropLast(1, union);
                    const newLast = {
                        start: lastInUnion.start,
                        end: currRange.end
                    };
                    return unionWithoutLast.concat([newLast]);
                }
                return union.concat(currRange);
            }
            return [currRange];
        }, []);
    }
    unIntersectRanges(sorted) {
        let previousRange;
        return sorted.reduce((union, currRange) => {
            const lastInUnion = ramda_1.last(union);
            if (lastInUnion) {
                const currRangeWithoutPreviousRange = range_1.subtract(currRange, previousRange);
                previousRange = currRange;
                if (currRangeWithoutPreviousRange.length == 0) {
                    return union;
                }
                if (currRangeWithoutPreviousRange.length == 1) {
                    return union.concat([currRangeWithoutPreviousRange[0]]);
                }
            }
            previousRange = currRange;
            return [currRange];
        }, []);
    }
}
exports.NaivePrioritySet = NaivePrioritySet;
;
//# sourceMappingURL=priority.set.naive.js.map