"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mockRanges = void 0;
const MIN = 0;
const MAX = 100000;
const MAX_DURATION = 6000;
const mockRanges = (amount) => Array.from({ length: amount }).map(randomRange);
exports.mockRanges = mockRanges;
const randomRange = () => {
    const start = Math.floor((Math.random() * (MAX - MIN)) + MIN);
    const length = Math.floor(Math.random() * MAX_DURATION);
    return {
        start,
        end: start + length
    };
};
//# sourceMappingURL=mocks.js.map