"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mocks_1 = require("./mocks");
const priority_set_fast_1 = require("./priority.set.fast");
const priority_set_naive_1 = require("./priority.set.naive");
const jsonexport_1 = __importDefault(require("jsonexport"));
const fs_1 = require("fs");
const bench_1 = require("./bench");
bench_1.benchmark("/Users/matanshaked/development/rust/hello_world/intervals.json");
// benchmarkDifferentAmounts();
function benchmarkDifferentAmounts() {
    const REPETITIONS = 6;
    const amounts = Array.from({ length: 300 }).map((_, idx) => idx * 100);
    const uniteTwoRangesWithAmount = (amount) => {
        const ranges1 = mocks_1.mockRanges(amount);
        const ranges2 = mocks_1.mockRanges(amount);
        const start = new Date().getTime();
        const set1 = new priority_set_fast_1.FastPrioritySet(ranges1.map(x => ({ data: {}, priority: 0, start: x.start, end: x.end })));
        const set2 = new priority_set_fast_1.FastPrioritySet(ranges2.map(x => ({ data: {}, priority: 1, start: x.start, end: x.end })));
        const union = set1.unite(set2);
        const end = new Date().getTime();
        const startNaive = new Date().getTime();
        const set1Naive = new priority_set_naive_1.NaivePrioritySet(ranges1.map(x => ({ data: {}, priority: 0, start: x.start, end: x.end })));
        const set2Naive = new priority_set_naive_1.NaivePrioritySet(ranges2.map(x => ({ data: {}, priority: 1, start: x.start, end: x.end })));
        const unionNaive = set1Naive.unite(set2Naive);
        const endNaive = new Date().getTime();
        return {
            amount,
            durationNaive: endNaive - startNaive,
            durationFast: end - start
            // unionNaive,
            // unionFast: union
        };
    };
    const results = amounts.map(amount => {
        console.log(`processing amount ${amount}`);
        const repetitionsResults = Array.from({ length: REPETITIONS }).map(() => uniteTwoRangesWithAmount(amount));
        const totalRepetitionsResults = repetitionsResults.reduce((acc, curr) => ({
            amount: curr.amount,
            durationFast: acc.durationFast + curr.durationFast,
            durationNaive: acc.durationNaive + curr.durationNaive
        }));
        return {
            amount,
            durationFast: totalRepetitionsResults.durationFast / REPETITIONS,
            durationNaive: totalRepetitionsResults.durationNaive / REPETITIONS
        };
    });
    jsonexport_1.default(results, (err, csv) => {
        fs_1.writeFileSync("./results.csv", csv);
    });
    results.map(result => {
        console.log(`amount=${result.amount}
    fast took ${result.durationFast} ms
    naive took ${result.durationNaive} ms
    `);
    });
    const totalDurations = results.reduce((acc, curr) => ({
        totalDurationFast: acc.totalDurationFast + curr.durationFast,
        totalDurationNaive: acc.totalDurationNaive + curr.durationNaive
    }), { totalDurationNaive: 0, totalDurationFast: 0 });
    console.log(`avg fast result is ${totalDurations.totalDurationFast / results.length}`);
    console.log(`avg naive result is ${totalDurations.totalDurationNaive / results.length}`);
}
//# sourceMappingURL=index.js.map